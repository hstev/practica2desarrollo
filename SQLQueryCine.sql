GO
CREATE DATABASE cine;

GO

USE cine;

GO

CREATE TABLE pelicula (
idPelicula INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
titulo TEXT NOT NULL,
fechaEstreno TEXT NULL,
clasificacion TEXT NULL,
sinopsis TEXT NULL,
pais TEXT NULL,
duracion TEXT NULL,
reparto TEXT NULL,
director TEXT NULL,
genero TEXT NULL,
idioma TEXT NULL,
imagen TEXT NULL
);

GO

INSERT INTO pelicula (titulo, fechaEstreno, clasificacion, sinopsis, pais, duracion, reparto, director, genero, idioma, imagen)
VALUES
('RAMPAGE', 'Jueves, Abril 12, 2018', 'RECOMENDADA PARA MAYORES DE 7 A�OS', 'Davis Okoye es un experto en primates y un hombre que se mantiene a distancia de la gente. Tiene un v�nculo entra�able con George �un gorila espalda plateada extraordinariamente inteligente� a quien ha cuidado desde que naci�. Cuando un experimento gen�tico se sale de control, provoca que George mute de ser un simio gentil a una criatura furiosa de proporciones monstruosas. Para acabar de empeorar la situaci�n, se descubre que hay m�s animales con alteraciones similares. Mientras estos nuevos depredadores alfa destruyen todo lo que encuentran a su paso por Estados Unidos, Davis Okoye busca un ant�doto junto con un ingeniero desacreditado. Ellos deber�n enfrentar un campo de batalla que cambia todo el tiempo no solamente para evitar una cat�strofe mundial, sino tambi�n para salvar a la criatura temeraria que alguna vez fue su amigo', 'Estados Unidos', '107 minutos', 'Dwayne The Rock Johnson, Jeffrey Dean Morgan, Malin Akerman, Joe Manganiello, Naomie Harris, Marley Shelton, Jake Lacy, Jack Quaid, Breanne Hill, Matt Gerald, P.J. Byrne, Destiny Lopez, Jason Liles, James Sterling, Michael David Yuhl, Allyssa Brooke, Daniel Craig Baker, Anthony Collins, Laura Distin, Giota Trakas, Mac Wells, Matthew Ewald, Pete Burris, Brad Spiers', 'Brad Peyton', 'Acci�n, Ciencia Ficci�n, Monstruos', 'Ingl�s', 'rampage5maezo2018.png'),
('AVENGERS: INFINITY WAR', 'Jueves, Abril 26, 2018', 'RECOMENDADA PARA MAYORES DE 12 A�OS', 'Como parte de un recorrido cinematogr�fico sin precedentes de diez a�os, de creaci�n y expansi�n del Universo Cinematogr�fico de Marvel, AVENGERS: INFINITY WAR trae a la pantalla la mayor confrontaci�n de todos los tiempos. Los Vengadores y sus S�per H�roes aliados deber�n estar dispuestos a sacrificarlo todo para derrotar al poderoso Thanos antes de que �l destruya el universo.', 'Estados Unidos', '', 'Robert Downey Jr.; Chris Patt; Chris Evans; Chris Hemsworth; Scarlett Johansson; Josh Brolin; Benedict Cumberbatch; Tom Holland; Elizabeth Olsen; Zoe Salda�a; Mark Ruffalo; Karen Gillan; Jeremy Renner; Vin Diesel; Dave Bautista; Paul Bettany; Sebastian Stan; Stan lee, Danai Gurira; Tom Hiddleston; Benicio del Toro; Anthony Mackie; Chadwick Boseman; Paul Rudd; Don Cheadle.', 'Anthony y Joe Russo', 'Acci�n, Aventura', 'Ingl�s', 'avengersinfinitywar20de-marzo20178.jpg'),
('PETER RABBIT', 'Jueves, Marzo 15, 2018', 'APTA PARA TODO EL P�BLICO', 'Peter Rabbit, el travieso y aventurero h�roe que ha cautivado a generaciones de lectores, ahora interpreta el papel protag�nico en su propia comedia contempor�nea. En la pel�cula, la contienda de Peter con el Sr. McGregor (Domhnall Gleeson) escala a mayores alturas cuando sus peleas para lograr el control de la codiciada huerta de vegetales de McGregor y el afecto de la bondadosa vecina que ama a los animales (Rose Byrne) se extiende a Lake District y a Londres. James Corden da voz al personaje de Peter con esp�ritu juguet�n y encanto silvestre.', 'Reino Unido / Australia / Estados Unidos', '95 minutos', 'James Corden, Rose Byrne, Domhnall Gleeson, Margot Robbie, Elizabeth Debickiy Daisy Ridley', 'Will Gluck', 'Animaci�n', 'Ingl�s', 'peterrabbit8febrero2018.jpg'),
('SHERLOCK GNOMES', 'Jueves, Marzo 22, 2018', 'APTA PARA TODO EL P�BLICO', 'Los amados nomos de la pel�cula GNOMEO Y JULIET est�n de vuelta para una nueva aventura en Londres. Cuando Gnomeo y Juliet llegan por primera vez a la ciudad con su familia y amigos, su mayor preocupaci�n es preparar el nuevo jard�n para la primavera. Pero pronto descubren que alguien est� secuestrando los nomos en todo Londres. Ahora solo hay un nomo para llamar, SHERLOCK GNOMES. El famoso detective llega con su compa�ero Watson para investigar el caso. El misterio los conducir� a una aventura desenfrenada donde conocer�n ornamentos nuevos y explorar�n un lado desconocido de la ciudad. ', 'Estados Unidos', '86 minutos', 'James McAvoy, Emily Blunt, Chiwetel Ejiofor, Maggie Smith, Michael Caine, Ashley Jensen, Matt Lucas, Stephen Merchant, Mary J. Blige and Johnny Depp', 'John Stevenson', 'Animaci�n, Comedia', 'Ingl�s', 'sherlockgnomes20febrero2018.jpg'),
('TRUTH OR DARE', 'Jueves, Mayo 3, 2018', 'APTA PARA MAYORES DE 15 A�OS', 'Lucy Hale (Pretty Little Liars) y Tyler Posey (Teen Wolf) encabezan el reparto de Verdad o Reto, una pel�cula de suspenso con toques sobrenaturales de Blumhouse Productions (Feliz D�a de tu Muerte, Huye, Fragmentado). Un juego inofensivo de �Verdad o Reto� entre amigos se torna mortal cuando algo o alguien comienza a castigar a aquellos que dicen una mentira o se reh�san a cumplir el reto. Dirigida por Jeff Wadlow (Kick-Ass 2), el resto del reparto incluye a Violett Beane, Hayden Szeto, Landon Liboiron, Sophia Taylor Ali y Nolan Gerard Funk. La pel�cula es producida por Jason Blum de Blumhouse, y como productores ejecutivos est�n Wadlow, Chris Roach, Jeanette Volturno y Couper Samuelson.', 'Estados Unidos', '100 minutos', 'Lucy Hale, Tyler Posey, Violett Beane, Hayden Szeto, Landon Liboiron, Sophia Taylor Ali, Nolan Gerard Funk', 'Jeff Wadlow', 'Thriller Supernatural', 'Ingl�s', 'verdadoreto2abril2018.jpg');

GO

CREATE TABLE programacion(
idProgramacion INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
fkPelicula INT FOREIGN KEY REFERENCES pelicula (idPelicula),
fecha DATE NOT NULL,
horaInicio TIME NOT NULL,
horaFin TIME NOT NULL,
);

GO

INSERT INTO programacion
VALUES
(1, '2018-05-15', '20:00:00', '22:00:00'),
(2, '2018-05-15', '18:00:00', '20:00:00'),
(3, '2018-05-15', '16:00:00', '18:00:00'),
(4, '2018-05-15', '14:00:00', '16:00:00'),
(5, '2018-05-15', '12:00:00', '14:00:00'),
(1, '2018-05-16', '20:00:00', '22:00:00'),
(2, '2018-05-16', '18:00:00', '20:00:00'),
(3, '2018-05-16', '16:00:00', '18:00:00'),
(4, '2018-05-16', '14:00:00', '16:00:00'),
(5, '2018-05-16', '12:00:00', '14:00:00'),
(1, '2018-05-17', '20:00:00', '22:00:00'),
(2, '2018-05-17', '18:00:00', '20:00:00'),
(3, '2018-05-17', '16:00:00', '18:00:00'),
(4, '2018-05-17', '14:00:00', '16:00:00'),
(5, '2018-05-17', '12:00:00', '14:00:00');

GO
 
CREATE TABLE rol(
idRol INT NOT NULL PRIMARY KEY,
rol VARCHAR(20) NOT NULL
);

GO

INSERT INTO rol 
VALUES 
(1, 'ADMIN'),
(2, 'CLIENTE'),
(3, 'CLIENTE');

GO

CREATE TABLE usuario(
cedula INT NOT NULL PRIMARY KEY,
nombre VARCHAR(50) NOT NULL,
apellido VARCHAR (50) NULL,
fkRol INT FOREIGN KEY REFERENCES rol (idRol),
correo VARCHAR(100) NOT NULL,
usuario VARCHAR(20) NOT NULL,
clave VARCHAR(20) NOT NULL
);

GO

INSERT INTO usuario 
VALUES
(1234567891, 'Harold', 'Restrepo', 1, 'harold@email.com','harold', 'harold123'),
(1789654232, 'Pepito', 'Perez', 2, 'pepito@email.com','pepito', 'pepito123'),
(1478596323, 'Monica', 'Sierra', 2, 'monica@email.com','monica', 'monica123'),
(1427589654, 'Gabriel', 'Montiel', 1, 'gabriel@email.com','gabriel', 'gabriel123'),
(1225000505, 'Mark', 'Zuckerberg', 2,'mark@email.com','mark', 'mark123'),
(1252225636, 'Elon', 'Musk', 2, 'elon@email.com', 'elon', 'elon123'),
(1000555507, 'Bill', 'Gates', 2, 'bill@email.com', 'bill', 'bill123'),
(1255599688, 'Linus', 'Torvalds', 2, 'linus@email.com', 'linus', 'linus123'),
(1455554849, 'Nikola', 'Tesla', 2, 'nikola@email.com', 'nikola', 'nikola123'),
(1225223330, 'Henry', 'Ford' , 2, 'henry@email.com', 'henry', 'henry123'),
(1111477851, 'Albert', 'Einstein', 3, 'albert@email.com', 'albert', 'albert123'),
(1040558236, 'Ada', 'Lovelace', 2, 'ada@email.com', 'ada', 'ada123');

GO

CREATE TABLE reserva(
idReserva INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
fkProgramacion INT FOREIGN KEY REFERENCES programacion (idProgramacion),
fkUsuario INT FOREIGN KEY REFERENCES usuario (cedula),
fechaReserva DATE NOT NULL,
pago INT NOT NULL,
precio MONEY NULL,
puesto INT NULL
);

GO
INSERT INTO reserva
VALUES
(1, 1478596323, SYSDATETIME(), 1, (17000+2700), 1),
(2, 1478596323, SYSDATETIME(), 1, (17000+2700), 15),
(3, 1478596323, SYSDATETIME(), 1, (17000+2700), 33),
(5, 1000555507, SYSDATETIME(), 1, (17000+2700), 36),
(6, 1000555507, SYSDATETIME(), 1, (17000+2700), 52),
(7, 1000555507, SYSDATETIME(), 1, (17000+2700), 51),
(11, 1040558236, SYSDATETIME(), 1, (17000+2700), 26),
(12, 1040558236, SYSDATETIME(), 1, (17000+2700), 22),
(15, 1040558236, SYSDATETIME(), 1, (17000+2700), 55),
(1, 1789654232, SYSDATETIME(), 1, (17000+2700), 44),
(3, 1789654232, SYSDATETIME(), 1, (17000+2700), 13),
(5, 1789654232, SYSDATETIME(), 1, (17000+2700), 20),
(1, 1255599688, SYSDATETIME(), 1, (17000+2700), 12),
(7, 1255599688, SYSDATETIME(), 1, (17000+2700), 50),
(13, 1255599688, SYSDATETIME(), 1, (17000+2700), 60);