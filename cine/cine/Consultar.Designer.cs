﻿namespace cine
{
    partial class Consultar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Consultar));
            this.label1 = new System.Windows.Forms.Label();
            this.txtNomUser = new System.Windows.Forms.TextBox();
            this.lbpuesto = new System.Windows.Forms.Label();
            this.cbPuesto = new System.Windows.Forms.ComboBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lbMsj = new System.Windows.Forms.Label();
            this.dgConsulta = new System.Windows.Forms.DataGridView();
            this.btnCancelarRes = new System.Windows.Forms.Button();
            this.btnCambiarPuesto = new System.Windows.Forms.Button();
            this.cbCambiarPuesto = new System.Windows.Forms.ComboBox();
            this.pnAccion = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbhelp = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgConsulta)).BeginInit();
            this.pnAccion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(22, 168);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre de usuario";
            // 
            // txtNomUser
            // 
            this.txtNomUser.Location = new System.Drawing.Point(124, 165);
            this.txtNomUser.Name = "txtNomUser";
            this.txtNomUser.Size = new System.Drawing.Size(121, 20);
            this.txtNomUser.TabIndex = 1;
            // 
            // lbpuesto
            // 
            this.lbpuesto.AutoSize = true;
            this.lbpuesto.ForeColor = System.Drawing.Color.White;
            this.lbpuesto.Location = new System.Drawing.Point(66, 194);
            this.lbpuesto.Name = "lbpuesto";
            this.lbpuesto.Size = new System.Drawing.Size(40, 13);
            this.lbpuesto.TabIndex = 2;
            this.lbpuesto.Text = "Puesto";
            // 
            // cbPuesto
            // 
            this.cbPuesto.FormatString = "N2";
            this.cbPuesto.FormattingEnabled = true;
            this.cbPuesto.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59",
            "60"});
            this.cbPuesto.Location = new System.Drawing.Point(124, 191);
            this.cbPuesto.Name = "cbPuesto";
            this.cbPuesto.Size = new System.Drawing.Size(121, 21);
            this.cbPuesto.TabIndex = 3;
            this.cbPuesto.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.cbPuesto.TextChanged += new System.EventHandler(this.cbPuesto_TextChanged);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBuscar.Location = new System.Drawing.Point(95, 230);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 4;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(57, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "Consultar reserva";
            // 
            // lbMsj
            // 
            this.lbMsj.AutoSize = true;
            this.lbMsj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lbMsj.Location = new System.Drawing.Point(22, 276);
            this.lbMsj.Name = "lbMsj";
            this.lbMsj.Size = new System.Drawing.Size(0, 13);
            this.lbMsj.TabIndex = 8;
            // 
            // dgConsulta
            // 
            this.dgConsulta.AllowUserToAddRows = false;
            this.dgConsulta.AllowUserToDeleteRows = false;
            this.dgConsulta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgConsulta.Location = new System.Drawing.Point(279, 59);
            this.dgConsulta.Name = "dgConsulta";
            this.dgConsulta.ReadOnly = true;
            this.dgConsulta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgConsulta.Size = new System.Drawing.Size(456, 172);
            this.dgConsulta.TabIndex = 9;
            this.dgConsulta.Visible = false;
            this.dgConsulta.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgConsulta_CellClick);
            // 
            // btnCancelarRes
            // 
            this.btnCancelarRes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelarRes.Location = new System.Drawing.Point(314, 8);
            this.btnCancelarRes.Name = "btnCancelarRes";
            this.btnCancelarRes.Size = new System.Drawing.Size(112, 23);
            this.btnCancelarRes.TabIndex = 10;
            this.btnCancelarRes.Text = "Cancelar reserva";
            this.btnCancelarRes.UseVisualStyleBackColor = true;
            this.btnCancelarRes.Click += new System.EventHandler(this.btnCancelarRes_Click);
            // 
            // btnCambiarPuesto
            // 
            this.btnCambiarPuesto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCambiarPuesto.Location = new System.Drawing.Point(178, 8);
            this.btnCambiarPuesto.Name = "btnCambiarPuesto";
            this.btnCambiarPuesto.Size = new System.Drawing.Size(103, 23);
            this.btnCambiarPuesto.TabIndex = 11;
            this.btnCambiarPuesto.Text = "Cambiar puesto";
            this.btnCambiarPuesto.UseVisualStyleBackColor = true;
            this.btnCambiarPuesto.Click += new System.EventHandler(this.btnCambiarPuesto_Click);
            // 
            // cbCambiarPuesto
            // 
            this.cbCambiarPuesto.FormattingEnabled = true;
            this.cbCambiarPuesto.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50"});
            this.cbCambiarPuesto.Location = new System.Drawing.Point(119, 9);
            this.cbCambiarPuesto.Name = "cbCambiarPuesto";
            this.cbCambiarPuesto.Size = new System.Drawing.Size(50, 21);
            this.cbCambiarPuesto.TabIndex = 12;
            // 
            // pnAccion
            // 
            this.pnAccion.Controls.Add(this.btnCancelarRes);
            this.pnAccion.Controls.Add(this.cbCambiarPuesto);
            this.pnAccion.Controls.Add(this.btnCambiarPuesto);
            this.pnAccion.Location = new System.Drawing.Point(288, 247);
            this.pnAccion.Name = "pnAccion";
            this.pnAccion.Size = new System.Drawing.Size(432, 42);
            this.pnAccion.TabIndex = 13;
            this.pnAccion.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::cine.Properties.Resources.search;
            this.pictureBox1.Location = new System.Drawing.Point(77, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(93, 92);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // lbhelp
            // 
            this.lbhelp.AutoSize = true;
            this.lbhelp.ForeColor = System.Drawing.Color.White;
            this.lbhelp.Location = new System.Drawing.Point(276, 33);
            this.lbhelp.Name = "lbhelp";
            this.lbhelp.Size = new System.Drawing.Size(223, 13);
            this.lbhelp.TabIndex = 15;
            this.lbhelp.Text = "Click en la reserva para mostrar mas acciones";
            this.lbhelp.Visible = false;
            // 
            // Consultar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(757, 334);
            this.Controls.Add(this.lbhelp);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pnAccion);
            this.Controls.Add(this.lbMsj);
            this.Controls.Add(this.dgConsulta);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.cbPuesto);
            this.Controls.Add(this.lbpuesto);
            this.Controls.Add(this.txtNomUser);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Consultar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultar";
            this.Load += new System.EventHandler(this.Consultar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgConsulta)).EndInit();
            this.pnAccion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNomUser;
        private System.Windows.Forms.Label lbpuesto;
        private System.Windows.Forms.ComboBox cbPuesto;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbMsj;
        private System.Windows.Forms.DataGridView dgConsulta;
        private System.Windows.Forms.Button btnCancelarRes;
        private System.Windows.Forms.Button btnCambiarPuesto;
        private System.Windows.Forms.ComboBox cbCambiarPuesto;
        private System.Windows.Forms.Panel pnAccion;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbhelp;
    }
}