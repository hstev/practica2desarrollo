﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace cine
{
    public partial class Consultar : Form
    {
        public Consultar(string usuario, int rol)
        {
            
            InitializeComponent();
            if (rol == 2)
            {
                txtNomUser.Text = usuario;
                txtNomUser.Enabled = false;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            lbMsj.Text = "";
            dgConsulta.Visible = false;
            string usuario;
            string puesto;
            usuario = txtNomUser.Text;
            puesto = cbPuesto.Text;

            if (usuario != "" && puesto != "")
            {
                int silla = Int32.Parse(puesto);
                DBDatos DBD = new DBDatos();
                SqlConnection conection;
                conection = DBD.conectar();
                string con = "SELECT res.idReserva, pel.titulo as Pelicula, pro.fecha as Fecha, pro.horaInicio as Empieza, pro.horaFin as Termina, res.puesto as Puesto FROM reserva res INNER JOIN programacion pro ON pro.idProgramacion = res.fkProgramacion INNER JOIN pelicula pel ON pel.idPelicula = pro.fkPelicula INNER JOIN usuario usu ON usu.cedula = res.fkUsuario WHERE usu.usuario = '"+usuario+"' AND res.puesto = "+silla;
                SqlDataReader tabla = DBD.consulta(con, conection);
                
                if (tabla.Read())
                {
                    tabla.Close();
                    var dataAdapter = new SqlDataAdapter(con, conection);
                    var ds = new DataSet();
                    dataAdapter.Fill(ds);
                    dgConsulta.ReadOnly = true;
                    dgConsulta.DataSource = ds.Tables[0];
                    this.dgConsulta.Columns["idReserva"].Visible = false;
                    dgConsulta.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    dgConsulta.Visible = true;
                    lbhelp.Visible = true;
                }
                else
                {
                    pnAccion.Visible = false;
                    dgConsulta.Visible = false;
                    lbhelp.Visible = false;
                    lbMsj.Text = "No se encuentran resultados con la busqueda usuario: "+usuario+" y puesto: "+puesto;
                }
            }
            else
            {
                lbMsj.Text = "Por favor, completa todos los campos para realizar la busqueda";
                pnAccion.Visible = false;
                dgConsulta.Visible = false;
                lbhelp.Visible = false;
            }
        }

        private void btnCerrar2_Click(object sender, EventArgs e)
        {

        }

        private void cbPuesto_TextChanged(object sender, EventArgs e)
        {

        }

        private void Consultar_Load(object sender, EventArgs e)
        {

        }

        private void dgConsulta_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            pnAccion.Visible = true;
        }

        private void btnCancelarRes_Click(object sender, EventArgs e)
        {
            int idRes = Int32.Parse(dgConsulta.CurrentRow.Cells["idReserva"].Value.ToString());
            
            DBDatos DBD = new DBDatos();
            SqlConnection conection;
            conection = DBD.conectar();
            string con = "DELETE FROM reserva WHERE idReserva = "+idRes;
            int n = DBD.operar(con, conection);
            if (n > 0)
            {
                lbMsj.Text = "Se ha cancelado la reserva";
                dgConsulta.Visible = false;
                pnAccion.Visible = false;
                lbhelp.Visible = false;
            }
            else
            {
                lbMsj.Text = "Hubo un error, no pudimos cancelar tu reserva";
                dgConsulta.Visible = false;
                pnAccion.Visible = false;
                lbhelp.Visible = false;
            }
        }

        private void btnCambiarPuesto_Click(object sender, EventArgs e)
        {
            if (cbCambiarPuesto.Text != "") 
            {
                int idRes = Int32.Parse(dgConsulta.CurrentRow.Cells["idReserva"].Value.ToString());
                int puesto = Int32.Parse(cbCambiarPuesto.Text);
                DBDatos DBD = new DBDatos();
                SqlConnection conection;
                conection = DBD.conectar();
                string con = "UPDATE reserva SET puesto = " + puesto + " WHERE idReserva = " + idRes;
                int n = DBD.operar(con, conection);
                if (n > 0)
                { 
                    lbMsj.Text = "Se ha actualizado el puesto de reserva";
                    dgConsulta.Visible = false;
                    pnAccion.Visible = false;
                    lbhelp.Visible = false;
                }
                else
                {
                    lbMsj.Text = "Hubo un error, no pudimos cambiar el puesto";
                    dgConsulta.Visible = false;
                    pnAccion.Visible = false;
                    lbhelp.Visible = false;
                }
            }
            else
            {
                lbMsj.Text = "No has seleccionado un puesto";
            }
        }
    }
}
