﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace cine
{
    class DBDatos
    {
        public SqlConnection conectar()
        {
            SqlConnection conectar = new SqlConnection(@"Data Source=localhost;Initial Catalog=cine;Integrated Security=SSPI;");
            try
            {
                conectar.Open();
                //MessageBox.Show("Se realizo la conexion...");
                return conectar;
            }
            catch
            {
                //MessageBox.Show("Falló la conexión " + ex.ToString());
                return null;
            }
        }

        public SqlDataReader consulta(string conSQL, SqlConnection conector)
        {
            try
            {
                SqlCommand comando = new SqlCommand(conSQL, conector);
                SqlDataReader tabla = comando.ExecuteReader();
                return tabla;
            }
            catch 
            {
                //MessageBox.Show("Falló la consulta " + ex.ToString());
                return null;
            }
        }

        public int operar(string conSQL, SqlConnection conector)
        {
            int num = 0;
            try
            {
                SqlCommand comando = new SqlCommand(conSQL, conector);
                num = comando.ExecuteNonQuery();
                return num;
            }
            catch 
            {
                //MessageBox.Show("Falló la consulta" + e.ToString());
                return num;
            }
        }

        public void cerrar(SqlConnection conector)
        {
            try
            {
                conector.Close();
            }
            catch  { }
        }
    }
}

