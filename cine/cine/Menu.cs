﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cine
{
    public partial class Menu : Form
    {
        int currentRol, currentCc;
        string currentUser;

        public Menu(int rol, int cc, string usuario)
        {
            this.currentCc = cc;
            this.currentRol = rol;
            this.currentUser = usuario;

            InitializeComponent();
            switch (rol)
            {
                case 1:
                    tsReserva.Visible = false;
                    break;
                case 2:
                    tsReportes.Visible = false;
                    tsVenderTiquete.Visible = false;
                    break;
                case 3:
                    tsReserva.Visible = false;
                    tsReportes.Visible = false;
                    break;
            }
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void Menu_Load(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void salirToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void realizarReservacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActualizarR a = new ActualizarR();
            a.MdiParent = this;
            a.Show();
        }

        private void consultarPorNombreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Consultar c = new Consultar(this.currentUser, this.currentRol);
            c.MdiParent = this;
            c.Show();
        }

        private void archivoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void venderTiqueteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VenderTiquete v = new VenderTiquete();
            v.MdiParent = this;
            v.Show();
        }

        private void salirToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void porNombreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Consultar c = new Consultar(this.currentUser, this.currentRol);
            c.MdiParent = this;
            c.Show();
        }

        private void peliculasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Peliculas p = new Peliculas();
            p.MdiParent = this;
            p.Show();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clientes c = new Clientes();
            c.MdiParent = this;
            c.Show();
        }

        private void programacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Programacion pr = new Programacion();
            pr.MdiParent = this;
            pr.Show();
        }

        private void realizarReservaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reserva r = new Reserva(currentCc);
            r.MdiParent = this;
            r.Show();
        }
    }
}
