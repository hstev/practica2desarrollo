﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cine
{
    public partial class Peliculas : Form
    {
        public Peliculas()
        {
            InitializeComponent();
        }

        private void Peliculas_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'DataSetPeliculas.pelicula' Puede moverla o quitarla según sea necesario.
            this.peliculaTableAdapter.Fill(this.DataSetPeliculas.pelicula);

            this.reportViewer1.RefreshReport();
        }
    }
}
