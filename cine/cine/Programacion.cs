﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cine
{
    public partial class Programacion : Form
    {
        public Programacion()
        {
            InitializeComponent();
        }

        private void Programacion_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'cineDataSet.reserva' Puede moverla o quitarla según sea necesario.
            this.reservaTableAdapter.Fill(this.cineDataSet.reserva);
            // TODO: esta línea de código carga datos en la tabla 'cineDataSet.programacion' Puede moverla o quitarla según sea necesario.
            this.programacionTableAdapter.Fill(this.cineDataSet.programacion);


            //this.reportViewer1.RefreshReport();
        }
    }
}
