﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace cine
{
    public partial class Registro : Form
    {
 
        DBDatos DBD = new DBDatos();
        SqlConnection conection;

        public Registro()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nombre, cedula, apellido, correo, usuario, clave;
  
            cedula = txtCedula.Text;
            nombre = txtNombre.Text;
            apellido = txtApellido.Text;
            correo = txtCorreo.Text;
            usuario = txtUsuario.Text;
            clave = txtClave.Text;

            if (cedula != "" &&  nombre != "" && apellido != "" && correo != "" && usuario != "" && clave != "")
            {
                int cc = Int32.Parse(cedula);

                conection = DBD.conectar();
                string con = "INSERT INTO usuario (cedula, nombre, apellido, fkRol, correo, usuario, clave) VALUES ("+cc+", '" +nombre+ "', '" + apellido+ "', 2, '" +correo+ "', '" +usuario+ "', '" +clave+ "');";
                int n = DBD.operar(con, conection);
                if (n > 0)
                {
                    txtCedula.Enabled = false;
                    txtNombre.Enabled = false;
                    txtApellido.Enabled = false;
                    txtCorreo.Enabled = false;
                    txtUsuario.Enabled = false;
                    txtClave.Enabled = false;
                    lbMsj.Text = "Usuario registrado con exito, puedes cerrar esta ventana e ingresar";
                }
                else
                {
                    lbMsj.Text = "La cedula ("+cedula+") ya está registrada";
                }
            }
            else
            {
                lbMsj.Text = "Por favor ingresa todos los datos para registrarte";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtNombre.ResetText();
            txtCedula.ResetText();
            txtApellido.ResetText();
            txtCorreo.ResetText();
            txtUsuario.ResetText();
            txtClave.ResetText();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtCedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!Char.IsNumber(e.KeyChar)){

                e.Handled = true;
            }
        }

        private void txtCedula_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
            
                e.Handled = true;
            }

        private void txtApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    
    }
}
