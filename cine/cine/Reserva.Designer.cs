﻿namespace cine
{
    partial class Reserva
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Reserva));
            this.label1 = new System.Windows.Forms.Label();
            this.dtFecha = new System.Windows.Forms.DateTimePicker();
            this.dgProgramacion = new System.Windows.Forms.DataGridView();
            this.cbopuestos = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnReservar = new System.Windows.Forms.Button();
            this.lbInfo = new System.Windows.Forms.Label();
            this.pnProgramacion = new System.Windows.Forms.Panel();
            this.lbMsj = new System.Windows.Forms.Label();
            this.pnDetalle = new System.Windows.Forms.Panel();
            this.lbClasi = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbGenero = new System.Windows.Forms.Label();
            this.lbTituloPel = new System.Windows.Forms.Label();
            this.pbPelicula = new System.Windows.Forms.PictureBox();
            this.pnSillas = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbResp = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgProgramacion)).BeginInit();
            this.pnProgramacion.SuspendLayout();
            this.pnDetalle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPelicula)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(68, 163);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(215, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Selecciona una fecha para hacer tu reserva";
            // 
            // dtFecha
            // 
            this.dtFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFecha.Location = new System.Drawing.Point(104, 187);
            this.dtFecha.Name = "dtFecha";
            this.dtFecha.Size = new System.Drawing.Size(132, 20);
            this.dtFecha.TabIndex = 1;
            this.dtFecha.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dgProgramacion
            // 
            this.dgProgramacion.AllowUserToAddRows = false;
            this.dgProgramacion.AllowUserToDeleteRows = false;
            this.dgProgramacion.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgProgramacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProgramacion.Location = new System.Drawing.Point(11, 12);
            this.dgProgramacion.MultiSelect = false;
            this.dgProgramacion.Name = "dgProgramacion";
            this.dgProgramacion.ReadOnly = true;
            this.dgProgramacion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProgramacion.Size = new System.Drawing.Size(297, 143);
            this.dgProgramacion.TabIndex = 2;
            this.dgProgramacion.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProgramacion_CellClick);
            this.dgProgramacion.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProgramacion_CellContentClick);
            // 
            // cbopuestos
            // 
            this.cbopuestos.Enabled = false;
            this.cbopuestos.FormattingEnabled = true;
            this.cbopuestos.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59",
            "60"});
            this.cbopuestos.Location = new System.Drawing.Point(84, 191);
            this.cbopuestos.Name = "cbopuestos";
            this.cbopuestos.Size = new System.Drawing.Size(57, 21);
            this.cbopuestos.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(33, 191);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Puesto";
            // 
            // btnReservar
            // 
            this.btnReservar.Enabled = false;
            this.btnReservar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnReservar.Location = new System.Drawing.Point(170, 191);
            this.btnReservar.Name = "btnReservar";
            this.btnReservar.Size = new System.Drawing.Size(75, 23);
            this.btnReservar.TabIndex = 7;
            this.btnReservar.Text = "Reservar";
            this.btnReservar.UseVisualStyleBackColor = true;
            this.btnReservar.Click += new System.EventHandler(this.btnReservar_Click);
            // 
            // lbInfo
            // 
            this.lbInfo.AutoSize = true;
            this.lbInfo.ForeColor = System.Drawing.Color.LightSalmon;
            this.lbInfo.Location = new System.Drawing.Point(33, 264);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(228, 13);
            this.lbInfo.TabIndex = 10;
            this.lbInfo.Text = "*Los asientos de la imagen son representativos";
            this.lbInfo.Visible = false;
            // 
            // pnProgramacion
            // 
            this.pnProgramacion.Controls.Add(this.lbResp);
            this.pnProgramacion.Controls.Add(this.label2);
            this.pnProgramacion.Controls.Add(this.lbInfo);
            this.pnProgramacion.Controls.Add(this.cbopuestos);
            this.pnProgramacion.Controls.Add(this.dgProgramacion);
            this.pnProgramacion.Controls.Add(this.btnReservar);
            this.pnProgramacion.Location = new System.Drawing.Point(12, 247);
            this.pnProgramacion.Name = "pnProgramacion";
            this.pnProgramacion.Size = new System.Drawing.Size(326, 283);
            this.pnProgramacion.TabIndex = 12;
            this.pnProgramacion.Visible = false;
            // 
            // lbMsj
            // 
            this.lbMsj.AutoSize = true;
            this.lbMsj.BackColor = System.Drawing.Color.Transparent;
            this.lbMsj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lbMsj.Location = new System.Drawing.Point(9, 219);
            this.lbMsj.Name = "lbMsj";
            this.lbMsj.Size = new System.Drawing.Size(0, 13);
            this.lbMsj.TabIndex = 11;
            this.lbMsj.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // pnDetalle
            // 
            this.pnDetalle.Controls.Add(this.lbClasi);
            this.pnDetalle.Controls.Add(this.label5);
            this.pnDetalle.Controls.Add(this.label4);
            this.pnDetalle.Controls.Add(this.lbGenero);
            this.pnDetalle.Controls.Add(this.lbTituloPel);
            this.pnDetalle.Controls.Add(this.pbPelicula);
            this.pnDetalle.Location = new System.Drawing.Point(918, 176);
            this.pnDetalle.Name = "pnDetalle";
            this.pnDetalle.Size = new System.Drawing.Size(239, 354);
            this.pnDetalle.TabIndex = 14;
            this.pnDetalle.Visible = false;
            // 
            // lbClasi
            // 
            this.lbClasi.AutoSize = true;
            this.lbClasi.BackColor = System.Drawing.Color.Transparent;
            this.lbClasi.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbClasi.ForeColor = System.Drawing.Color.White;
            this.lbClasi.Location = new System.Drawing.Point(15, 336);
            this.lbClasi.Name = "lbClasi";
            this.lbClasi.Size = new System.Drawing.Size(218, 12);
            this.lbClasi.TabIndex = 5;
            this.lbClasi.Text = "RECOMENDADO PARA MAYORES DE  12 AÑOS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(4, 321);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Clasificación:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(4, 303);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Genero:";
            // 
            // lbGenero
            // 
            this.lbGenero.AutoSize = true;
            this.lbGenero.ForeColor = System.Drawing.Color.White;
            this.lbGenero.Location = new System.Drawing.Point(55, 303);
            this.lbGenero.Name = "lbGenero";
            this.lbGenero.Size = new System.Drawing.Size(42, 13);
            this.lbGenero.TabIndex = 2;
            this.lbGenero.Text = "Genero";
            // 
            // lbTituloPel
            // 
            this.lbTituloPel.AutoSize = true;
            this.lbTituloPel.ForeColor = System.Drawing.Color.White;
            this.lbTituloPel.Location = new System.Drawing.Point(15, 17);
            this.lbTituloPel.Name = "lbTituloPel";
            this.lbTituloPel.Size = new System.Drawing.Size(73, 13);
            this.lbTituloPel.TabIndex = 1;
            this.lbTituloPel.Text = "Titulo Pelicula";
            // 
            // pbPelicula
            // 
            this.pbPelicula.Location = new System.Drawing.Point(36, 43);
            this.pbPelicula.Name = "pbPelicula";
            this.pbPelicula.Size = new System.Drawing.Size(170, 251);
            this.pbPelicula.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPelicula.TabIndex = 0;
            this.pbPelicula.TabStop = false;
            // 
            // pnSillas
            // 
            this.pnSillas.Location = new System.Drawing.Point(326, 163);
            this.pnSillas.Name = "pnSillas";
            this.pnSillas.Size = new System.Drawing.Size(590, 383);
            this.pnSillas.TabIndex = 15;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(918, -1);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(500, 160);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 13;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(48, 12);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(158, 112);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 11;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1, -1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(268, 150);
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::cine.Properties.Resources.cine_puestos;
            this.pictureBox1.Location = new System.Drawing.Point(263, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(730, 547);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // lbResp
            // 
            this.lbResp.AutoSize = true;
            this.lbResp.BackColor = System.Drawing.Color.Transparent;
            this.lbResp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lbResp.Location = new System.Drawing.Point(14, 222);
            this.lbResp.Name = "lbResp";
            this.lbResp.Size = new System.Drawing.Size(0, 13);
            this.lbResp.TabIndex = 11;
            // 
            // Reserva
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(60)))), ((int)(((byte)(61)))));
            this.ClientSize = new System.Drawing.Size(1169, 542);
            this.Controls.Add(this.pnSillas);
            this.Controls.Add(this.pnDetalle);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.lbMsj);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.dtFecha);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pnProgramacion);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Reserva";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reserva";
            this.Load += new System.EventHandler(this.Reserva_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgProgramacion)).EndInit();
            this.pnProgramacion.ResumeLayout(false);
            this.pnProgramacion.PerformLayout();
            this.pnDetalle.ResumeLayout(false);
            this.pnDetalle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPelicula)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtFecha;
        private System.Windows.Forms.DataGridView dgProgramacion;
        private System.Windows.Forms.ComboBox cbopuestos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnReservar;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lbInfo;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel pnProgramacion;
        private System.Windows.Forms.Label lbMsj;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel pnDetalle;
        private System.Windows.Forms.Label lbClasi;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbGenero;
        private System.Windows.Forms.Label lbTituloPel;
        private System.Windows.Forms.PictureBox pbPelicula;
        private System.Windows.Forms.Panel pnSillas;
        private System.Windows.Forms.Label lbResp;
    }
}