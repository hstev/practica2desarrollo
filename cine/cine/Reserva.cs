﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace cine
{
    public partial class Reserva : Form
    {
        int cedulaUsuario;
        public Reserva(int cc)
        {
            this.cedulaUsuario = cc;
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            lbMsj.Text = "";
            pnProgramacion.Visible = false;
            string fecha = DateTime.ParseExact(dtFecha.Text, "dd/MM/yyyy", null).ToString("yyyy-MM-dd");

            DBDatos DBD = new DBDatos();
            SqlConnection conection;
            conection = DBD.conectar();
            string con = "SELECT pro.idProgramacion, pel.clasificacion, pel.genero, pel.imagen, pel.titulo as Pelicula, pro.horaInicio as Empieza, pro.horaFin as Termina FROM programacion pro INNER JOIN pelicula pel ON pel.idPelicula = pro.fkPelicula WHERE pro.fecha = '"+fecha+"'";
            SqlDataReader tabla = DBD.consulta(con, conection);

            if (tabla.Read())
            {
                tabla.Close();
                var dataAdapter = new SqlDataAdapter(con, conection);
                var ds = new DataSet();
                dataAdapter.Fill(ds);
                dgProgramacion.ReadOnly = true;
                dgProgramacion.DataSource = ds.Tables[0];
                this.dgProgramacion.Columns["idProgramacion"].Visible = false;
                this.dgProgramacion.Columns["clasificacion"].Visible = false;
                this.dgProgramacion.Columns["genero"].Visible = false;
                this.dgProgramacion.Columns["imagen"].Visible = false;
                dgProgramacion.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                pnProgramacion.Visible = true;
            }
            else
            {
                pnProgramacion.Visible = false;
                lbMsj.Text = ":( Lo sentimos, No tenemos peliculas programadas para esta fecha.";
                pnDetalle.Visible = false;
                pnSillas.Visible = true;
            }
            DBD.cerrar(conection);

        }

        private void Reserva_Load(object sender, EventArgs e)
        {

        }

        private void dgProgramacion_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string clasificacion = dgProgramacion.Rows[e.RowIndex].Cells[1].Value.ToString();
            string genero = dgProgramacion.Rows[e.RowIndex].Cells[2].Value.ToString();
            string imagen = dgProgramacion.Rows[e.RowIndex].Cells[3].Value.ToString();
            string titulo = dgProgramacion.Rows[e.RowIndex].Cells[4].Value.ToString();
           
            lbGenero.Text = genero;
            lbTituloPel.Text = titulo;
            lbClasi.Text = clasificacion;
            string path = @"../../peliculas/"+imagen;
            pbPelicula.Image = new System.Drawing.Bitmap(path);
            pbPelicula.Load(path);
            pnSillas.Visible = false;
            pnDetalle.Visible = true;
            btnReservar.Enabled = true;
            btnReservar.Cursor = Cursors.Hand;
            cbopuestos.Enabled = true;
            lbInfo.Visible = true;
        }

        private void dgProgramacion_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void btnReservar_Click(object sender, EventArgs e)
        {
            

            if (cbopuestos.Text != "")
            {
                int idPro = Int32.Parse(dgProgramacion.CurrentRow.Cells["idProgramacion"].Value.ToString());
                int puesto = Int32.Parse(cbopuestos.Text);

                DBDatos DBD = new DBDatos();
                SqlConnection conection;
                conection = DBD.conectar();
                string con = "INSERT INTO reserva VALUES ("+idPro+", "+cedulaUsuario+", SYSDATETIME(), 1, 2700, "+puesto+")";
                int n = DBD.operar(con, conection);
                if (n > 0)
                {
                    lbResp.Text = "Se ha guardado tu reserva";
                }
                else
                {
                    lbResp.Text = "Hubo un error, no pudimos guardar tu reserva";
                }

            }
            else
            {
                lbResp.Text = "No has elegido un puesto para reservar";
            }
        }
    }
}
