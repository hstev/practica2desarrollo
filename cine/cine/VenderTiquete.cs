﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace cine
{
    public partial class VenderTiquete : Form
    {
        DBDatos DBD = new DBDatos();
        SqlConnection conection;
        
        public VenderTiquete()
        {
            InitializeComponent();
        }

        private void txtCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lbMsj.Text = "";
            string cedula = txtCliente.Text;
            
            if (cedula != "")
            {
                int cc = Int32.Parse(cedula);
                conection = DBD.conectar();
                string con = "SELECT CONCAT(nombre,' ', apellido) as nombre FROM usuario WHERE cedula = "+cc+" AND fkRol = 2";
                SqlDataReader tabla = DBD.consulta(con, conection);

                if (tabla.Read())
                {
                    lbCliente.Text = tabla[0].ToString();
                    conection = DBD.conectar();
                    string con2 = "SELECT pro.idProgramacion, pro.fecha as Fecha, pel.titulo as Pelicula, pro.horaInicio as Empieza, pro.horaFin as Termina FROM programacion pro INNER JOIN pelicula pel ON pel.idPelicula = pro.fkPelicula";
                    SqlDataReader tabla2 = DBD.consulta(con2, conection);

                    if (tabla2.Read())
                    {
                        tabla2.Close();
                        var dataAdapter = new SqlDataAdapter(con2, conection);
                        var ds = new DataSet();
                        dataAdapter.Fill(ds);
                        dgProgramacion.ReadOnly = true;
                        dgProgramacion.DataSource = ds.Tables[0];
                        this.dgProgramacion.Columns["idProgramacion"].Visible = false;

                        dgProgramacion.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                        lbIden.Text = cedula;
                        pnDetalle.Visible = true;

                    }
                    else
                    {
                        lbMsj.Text = "No se han encontrado peliculas programadas";
                        pnDetalle.Visible = false;
                        btnComprar.Enabled = false;
                        btnComprar.Cursor = Cursors.No;
                    }
                }
                else
                {
                    lbMsj.Text = "No se ha encontrado un cliente con ese numero de identificación";
                    pnDetalle.Visible = false;
                    btnComprar.Enabled = false;
                    btnComprar.Cursor = Cursors.No;
                }

            }
            else
            {
                lbMsj.Text = "Por favor ingresa un numero de identificación";
                pnDetalle.Visible = false;
            }
        }

        private void dgProgramacion_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnComprar.Enabled = true;
            btnComprar.Cursor = Cursors.Hand;
        }

        private void txtCliente_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnComprar_Click(object sender, EventArgs e)
        {
            int idPro = Int32.Parse(dgProgramacion.CurrentRow.Cells["idProgramacion"].Value.ToString());
            int cc = Int32.Parse(txtCliente.Text);

            DBDatos DBD = new DBDatos();
            SqlConnection conection;
            conection = DBD.conectar();
            string con = "INSERT INTO reserva VALUES (" + idPro + ", " + cc + ", SYSDATETIME(), 1, 19700, 0)";
            int n = DBD.operar(con, conection);
            if (n > 0)
            {
                lbMsj.Text = "Compra realizada con exito, imprimiendo ticket";
            }
            else
            {
                lbMsj.Text = "Hubo un error, no pudimos guardar la compra";
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            pnDetalle.Visible = false;
            txtCliente.Text = "";
        }

        private void dgProgramacion_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
